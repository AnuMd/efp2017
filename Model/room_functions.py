__author__ = 'anu'

import cv2,shutil,os,time, re, math, Image, glob
import numpy as np
from scipy import ndimage
from operator import itemgetter

from image import Image_class
from open_plan import planClass
from recompute import recompute
from connectedcomponent import CC
#---used only when we need old voronoi output
from voronoi import voronoi_class
from region_grow import region_grow_class
from line_optimization import line_optimization_class
from iterative_functions import iterative_functions_class
from generate_other_lines import generate_other_lines_class
from close_cont_detect_elements import close_cont_detect_elements_class


class room_functions_class:
    def __init__(self):
        self.iterative_obj = iterative_functions_class()


    def find_door_width(self,current_directory,orginal_image_path,only_image_name):
        # # ------testing section
        # # --english
        # # new_walls_path = current_directory+'/input_fps/new_walls/online_walls/'
        # # --french
        # new_walls_path = current_directory+'/input_fps/new_walls/walls_2/'
        # new_walls_list = os.listdir(new_walls_path)
        # for wall_room_num, wall_room in enumerate(new_walls_list):
        #     wall_image_name = str(wall_room)
        #     # --english
        #     # wall_name = wall_image_name[0:-4]
        #     # --french
        #     wall_name = wall_image_name[0:-11]
        #     if wall_name == only_image_name:
        #         # --english
        #         # self.orginal_image_path = new_walls_path+wall_name+'.png'
        #         # --french
        #         orginal_image_path = new_walls_path + wall_name + '_output.png'
        #         break
        # # --------end of testing section
        img = cv2.imread(orginal_image_path,cv2.IMREAD_COLOR)
        dst = cv2.inRange(img, (0,255,0), (0,255,0))
        labels, numlabels = ndimage.label(dst>127)
        avg_width = 0
        count = 0
        for i in range(1,numlabels+1):
            cc = CC(i)
            point= np.argwhere(labels == i)
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()
            cc.height= cc.y_max-cc.y_min+1
            cc.width= cc.x_max-cc.x_min+1
            cc.center_x = cc.y_min+(cc.height/2)
            cc.center_y = cc.x_min+(cc.width/2)
            cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
            cc.numPixel=(cc.image.sum()/255)
            cc.image = self.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            cc.find_contour()
            max_dist = cc.get_max_distance()
            if max_dist>20:
                avg_width+=max_dist
                count+=1
        if count > 0:
            avg_width = 2*avg_width/float(count)
        else:
            dst = cv2.inRange(img, (0,255,255),(0,255,255))
            labels, numlabels = ndimage.label(dst>127)
            avg_width = 0
            count = 0
            for i in range(1,numlabels+1):
                cc = CC(i)
                point= np.argwhere(labels == i)
                cc.y_min=point[:,0].min()
                cc.x_min=point[:,1].min()
                cc.y_max=point[:,0].max()
                cc.x_max=point[:,1].max()
                cc.height= cc.y_max-cc.y_min+1
                cc.width= cc.x_max-cc.x_min+1
                cc.center_x = cc.y_min+(cc.height/2)
                cc.center_y = cc.x_min+(cc.width/2)
                cc.image=(np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1])*255/i)
                cc.numPixel=(cc.image.sum()/255)
                cc.image = self.add_pad_single_pixel(cc.image)
                cc.set_coordinates()
                cc.find_contour()
                max_dist = cc.get_max_distance()
                if max_dist>20:
                    avg_width+=max_dist
                    count+=1
            if count != 0:
                if count > 2:
                    avg_width = avg_width/float(count) #a 0.50 penalty because windows are tipcally larger than doors
                else:
                    avg_width = 2*avg_width/float(count)
        if avg_width < 40:
            avg_width = 40
        return avg_width

    # --- cant call this method from imageOperations.py because it doesnt have a class and has only methods
    def add_pad_single_pixel(self, image):
        s = image.shape
        if len(s) == 2:
            z1 = np.zeros((1, s[1]))
            z2 = np.zeros((s[0] + 2, 1))
        else:
            z1 = np.zeros((1, s[1], s[2]))
            z2 = np.zeros((s[0] + 2, 1, s[2]))
        image = np.concatenate((z1, image), axis=0)
        image = np.concatenate((image, z1), axis=0)
        image = np.concatenate((z2, image), axis=1)
        image = np.concatenate((image, z2), axis=1)
        return image

    def all_room_functions(self,current_directory,path_output_d,path_output,
                           path_gravvitas,path_final_lines,avg_door_width,weight_list,
                           only_image_name,otsu,text_cordinates,special_cases_list,                              debug_mode):
        self.current_directory = current_directory
        self.path_output_d = path_output_d
        self.path_output = path_output
        self.path_gravvitas = path_gravvitas
        self.path_final_lines = path_final_lines


        # ----- line room selections
        inner_directory = self.path_output_d + 'Rooms/' + only_image_name + '/'
        openPlan = planClass(current_directory,self.path_output, inner_directory, otsu, only_image_name, True)

        # ------find all rooms : take all contours in floor plan, remove floor plan borders and select only room contours
        # start_room_find = time.time()
        all_rooms_img = openPlan.find_rooms(debug_mode)
        # find_rooms_time = time.time()
        # print 'room find time: ',find_rooms_time-start_room_find

        # ------Find Open Plan:get each room found from 'find_rooms()' and find each rooms number of text labels and store in num_labels_per_room
        # print 'find_open_plans'
        all_detected_contour_lines = []
        all_detected_contour_lines, text_data, no_of_rooms = openPlan.find_open_plans(text_cordinates, debug_mode,avg_door_width,all_detected_contour_lines,self.path_gravvitas,all_rooms_img)

        # find_rooms_time = time.time()
        # print 'room find time: ', find_rooms_time - start_room_find

        for text_row in text_data:
            if len(text_row[1]) > 1:
                for text_label_data in text_row[1]:
                    tx0, ty0, tx1, ty1 = text_label_data[2]
                    box_lines = [
                        [[tx0, ty0], [tx1, ty0]],
                        [[tx1, ty0], [tx1, ty1]],
                        [[tx1, ty1], [tx0, ty1]],
                        [[tx0, ty1], [tx0, ty0]]
                    ]
                    for r_num, cont_row in enumerate(all_detected_contour_lines):
                        if cont_row[0] == text_row[0]:
                            all_detected_contour_lines[r_num].append(
                                [text_label_data[0], text_label_data[1], text_label_data[2], box_lines])
                            break

        # --create list to store all open plan partition lines
        all_open_plan_lines = []

        # ----create text file to store open plan line data for evaluation
        op_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'w')

        # find_op_time = time.time()
        # print '--- find_op_time : ', find_op_time - find_rooms_time

        # ------store each open plan in 'OpenPlans' folder for future use
        open_plan_path = inner_directory + 'OpenPlans/'
        open_area_list = os.listdir(open_plan_path)
        open_area_list.sort()

        for room_num, room in enumerate(open_area_list):
            # if room_num ==1:
            # start_pre_time = time.time()
            image_name = str(room)
            room_number = image_name[10:-4]
            current_open_plan_contour_data = []
            for contour_detail_row in all_detected_contour_lines:
                if contour_detail_row[0] == int(room_number):
                    current_open_plan_contour_data = contour_detail_row
                    break
            # ---get only open plan name
            name = image_name[0:-4]
            # --write current open plan to a directory inside current iteration
            output_path = inner_directory + name + '/'
            if (os.path.exists(output_path)):
                shutil.rmtree(output_path)
            os.mkdir(output_path)
            os.mkdir(output_path + 'OpenPlans/')
            open_plan_image = cv2.imread(inner_directory + 'OpenPlans/' + image_name, cv2.IMREAD_GRAYSCALE)
            self.height, self.width = open_plan_image.shape
            cv2.imwrite(output_path + 'OpenPlans/' + image_name, open_plan_image)


            # -----create output folders to store intermediate steps
            os.mkdir(output_path + 'Edge_Extension_Lines/')
            os.mkdir(output_path + 'Voronoi_Lines/')
            os.mkdir(output_path + 'Line_Optimization/')
            best_line_path = output_path + 'Best_Lines/'
            os.mkdir(best_line_path)
            image_path = output_path + 'OpenPlans/' + image_name

            open_plan_text_cordinate = []
            text_words = []
            text_bounding_boxes = []
            open_plan_contour_lines = current_open_plan_contour_data[1]

            for r, row in enumerate(current_open_plan_contour_data[3:]):
                open_plan_text_cordinate.append(row[1])
                text_words.append([row[0], row[1]])
                text_bounding_boxes.append(row[2])

            if debug_mode:
                open_plan_test_image = open_plan_image.copy()
                for r,row in enumerate(text_words):
                    cord1 =row[1][0]
                    cord2 =row[1][1]
                    x_position = (len(row[0])/2)*25
                    text = str(row[0]).upper()
                    cv2.putText(open_plan_test_image, text, (cord1-x_position,cord2+12), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2,cv2.cv.CV_AA)
                cv2.imwrite(self.path_final_lines+'_Thesis_Step_01_'+only_image_name+'.png',open_plan_test_image)

            # end_pre_time = time.time()
            # print 'pre process time : ',end_pre_time-start_pre_time

            # start_ite_1_time = time.time()

            # #-----old voronoi diagram drawing
            # voronoi_obj = voronoi_class()
            # voronoi_obj.draw_voronoi(avg_door_width,image_name,text_words,open_plan_text_cordinate,open_plan_contour_lines,open_plan_image,path_final_lines)
            # print 'Voronoi diagram executed'

            original_edges_and_extensions = openPlan.extending_edges(avg_door_width, name, image_name,image_path,current_open_plan_contour_data,open_plan_text_cordinate,debug_mode)
            # end_edge_extension = time.time()
            # print 'edge extension time : ',end_edge_extension-end_pre_time

            generate_other_lines_obj = generate_other_lines_class()
            original_edges_and_extensions,interesting_lines,contour_angle_data = generate_other_lines_obj.generate_lines_from_interesting_points(only_image_name+image_name, self.path_final_lines,original_edges_and_extensions,open_plan_contour_lines,text_bounding_boxes, open_plan_text_cordinate,avg_door_width,self.height, self.width)
            # other_line_end = time.time()
            # print 'generate_lines_from_interesting_points : ',other_line_end-end_edge_extension

            # #--create orthogonal lines
            # generate_other_lines_obj.find_orthogonal_lines(open_plan_contour_lines,contour_angle_data)

            # print 'Grow_regions'
            # start_region = time.time()
            ring_obj = region_grow_class(output_path, open_plan_image, name)
            region_growed_image = ring_obj.grow_regions(current_open_plan_contour_data, open_plan_text_cordinate, name, image_name,image_path, debug_mode,text_words,text_bounding_boxes, path_final_lines,only_image_name)
            end_region = time.time()
            # print ('time for region grow', str(end_region - other_line_end))

            # print 'Get_new_lines'
            # start_get_new_lines = time.time()
            vornoi_data, all_contour_lines = ring_obj.get_new_lines(open_plan_text_cordinate, name, image_name,debug_mode,self.path_final_lines,text_words,region_growed_image)
            # end_get_new_lines = time.time()
            # print 'Get_new_lines Time : ', end_get_new_lines - end_region

            # print 'Shortest path calculation'
            # start_find_shortest_path = time.time()
            line_opt = line_optimization_class(output_path, best_line_path, only_image_name)
            shortest_path_data = line_opt.find_shortest_path_manhattan(vornoi_data, open_plan_text_cordinate,current_open_plan_contour_data, name, image_name, image_path, debug_mode,self.path_final_lines,text_words)
            # line_opt.test()
            # end_find_shortest_path = time.time()
            # print 'Shortest path calculation Time : ', end_find_shortest_path - end_get_new_lines

            # # print 'Find_edge_extensions_between_cordinates'
            # # start_Find_edge_extensions_between_cordinates = time.time()
            # edge_extensions_between_coordinates = line_opt.find_edge_extensions_between_cordinates(edge_extensions,
            #                                                                                        shortest_path_data,
            #                                                                                        open_plan_text_cordinate,
            #                                                                                        name, image_name,
            #                                                                                        debug_mode)
            # # end_Find_edge_extensions_between_cordinates = time.time()
            # # print 'Find_edge_extensions_between_cordinates Time ; ', end_Find_edge_extensions_between_cordinates - start_Find_edge_extensions_between_cordinates

            # debug_mode = True
            # print 'Give each Line a Score and Select Maximum Score'
            # start_max_line = time.time()
            max_line_list = []
            max_line = line_opt.find_lines_in_area(self.current_directory, current_open_plan_contour_data,avg_door_width, weight_list, vornoi_data, all_contour_lines,shortest_path_data, original_edges_and_extensions,interesting_lines,open_plan_text_cordinate, name,image_name, only_image_name,open_plan_path, room, text_words, text_bounding_boxes,self.path_final_lines,special_cases_list,debug_mode)
            max_line_list.append(max_line)
            # end_max_line = time.time()
            # print 'Give each Line a Score and Select Maximum Score Time : ', end_max_line - end_find_shortest_path
            # print 'Iteratoin 1 time : ', end_max_line-start_ite_1_time

            # ---------iterate over the process after setting max_score_line
            # -- Do it until partitions created by max_line doesnt have >1 text lables
            # print 'Iterate until complete Open plan is partitioned'
            # start_max_line_iteration = time.time()
            # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes =[],[],[]
            iter_open_plan_text_cordinate, iter_text_words, iter_text_bounding_boxes = open_plan_text_cordinate, text_words, text_bounding_boxes
            level = 2
            num_overall_plans = 1
            while num_overall_plans > 0:
                # iteration_time_start = time.time()
                num_overall_plans = 0
                # print '--------------Current Iteration is : ', level
                # ---first iteration over the loop follow this
                if level == 2:
                    # # # --********************* use when running only from 2nd iteration
                    # best_line_path1=self.current_directory+'/input_fps/max/'
                    # best_line_list=[]
                    # image_with_lines =0
                    # #---get image with max_line
                    # for file_l in os.listdir(best_line_path1):
                    #     if file_l.endswith(".png"):
                    #         best_line_list.append(file_l)
                    # if len(best_line_list)==1:
                    #     for img in best_line_list:
                    #         image_with_lines = img
                    # print 'path is....', best_line_path1+str(image_with_lines)
                    # #---read image with max_line
                    # input_image= cv2.imread(best_line_path1+str(image_with_lines),cv2.IMREAD_GRAYSCALE)
                    # #---define path to store max_line in current step
                    # final_output_path = best_line_path+str(level)+'/'
                    # os.mkdir(final_output_path)
                    # #---define path to store intermediate outputs
                    # new_output_path = best_line_path+'output_'+str(level)+'/'
                    # os.mkdir(new_output_path)
                    # #--new addition
                    # # print 'text_words'
                    # # for row in text_words:
                    # #     print row
                    # # print 'open_plan_text_cordinate'
                    # # for row in open_plan_text_cordinate:
                    # #     print row
                    # # print 'text_bounding_boxes'
                    # # for row in text_bounding_boxes:
                    # #     print row
                    #
                    # # ret,thresh = cv2.threshold(input_image,0,255,1)
                    # # contours,hierachy = cv2.findContours(thresh,1,2)
                    # # text_data_iterate =[]
                    # # for cnt_number,contour in enumerate(contours):
                    # #     text_count = 0
                    # #     temp_text_list = []
                    # #     for tr, text_cordinate in enumerate(open_plan_text_cordinate):
                    # #         if cv2.pointPolygonTest(contour,(text_cordinate[0],text_cordinate[1]),False)==1:
                    # #             text_count += 1
                    # #             temp_text_list.append(tr)
                    # #     text_data_iterate.append([cnt_number,temp_text_list,text_count])
                    # #
                    # # # print 'main text_data_iterate'
                    # # # for row in text_data_iterate:
                    # # #     print row
                    # #
                    # # text_data_iterate.sort(key=itemgetter(2),reverse=True)
                    # # for t_num,text_data_row in enumerate(text_data_iterate):
                    # #     #----remove first one -> one with highest num of text labels-> whihc means outer contour
                    # #     if t_num>0:
                    # #     # if text_data_row[2]>1:
                    # #         for textID in text_data_row[1]:
                    # #             iter_text_words.append(text_words[textID])
                    # #             iter_open_plan_text_cordinate.append(open_plan_text_cordinate[textID])
                    # #             iter_text_bounding_boxes.append(text_bounding_boxes[textID])
                    # # # ---********************** end running from 2nd iteration



                    # # --********************* used when running from begining
                    # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = open_plan_text_cordinate,text_words,text_bounding_boxes
                    # best_line_list = []
                    image_with_lines_path = ''
                    # ---get image with max_line
                    png_list = (glob.glob(best_line_path + '*.png'))
                    # for file_l in os.listdir(best_line_path):
                    #     if file_l.endswith(".png"):
                    #         best_line_list.append(file_l)
                    if len(png_list) == 1:
                        image_with_lines_path = png_list[0]
                        # for img in best_line_list:
                        #     image_with_lines = img
                    # ---read image with max_line
                    input_image = cv2.imread(image_with_lines_path, cv2.IMREAD_GRAYSCALE)
                    # ---define path to store max_line in current step
                    final_output_path = best_line_path + str(level) + '/'
                    os.mkdir(final_output_path)
                    # ---define path to store intermediate outputs
                    new_output_path = best_line_path + 'output_' + str(level) + '/'
                    os.mkdir(new_output_path)
                    # # --********************* end used when running from begining

                    # ---recompute finding max_line for current 'image with max_line'
                    orginal_image_name = image_with_lines_path.split('/')[-1][0:-4]
                    recompute_obj = recompute(special_cases_list)
                    # number_of_open_plans,max_lines,iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = recompute_obj.find_open_plan_area(orginal_image_name,avg_door_width,weight_list,input_image,final_output_path,new_output_path,iter_open_plan_text_cordinate,iter_text_words,level,iter_text_bounding_boxes,debug_mode)
                    number_of_open_plans, max_lines = recompute_obj.find_open_plan_area(self.current_directory,orginal_image_name,avg_door_width, weight_list,input_image, final_output_path,new_output_path,iter_open_plan_text_cordinate,iter_text_words, level,iter_text_bounding_boxes,debug_mode)
                    # ---store max_line details
                    if max_lines != 0:
                        for m_row in max_lines:
                            max_line_list.append(m_row)
                    if number_of_open_plans > 0:
                        num_overall_plans = num_overall_plans + 1

                # ---all other iterations follow 'else'
                else:
                    # print '-----------------------iteration 03'
                    # open_plan_text_cordinate,text_words
                    level2 = level - 1
                    # ---define path for earlier outputs
                    earlier_outputs = os.listdir(best_line_path + str(level2) + '/')
                    earlier_outputs.sort()
                    # ---define path to store max_line in current step
                    final_output_path = best_line_path + str(level) + '/'
                    os.mkdir(final_output_path)
                    # ---define path to store intermediate outputs
                    new_output_path = best_line_path + 'output_' + str(level) + '/'
                    os.mkdir(new_output_path)
                    c = 0
                    # --take each image and recompute
                    new_weight = False
                    # op_cord,op_word,op_bb = iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes
                    for image_with_lines in earlier_outputs:
                        # iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = open_plan_text_cordinate,text_words,text_bounding_boxes

                        input_image = cv2.imread(best_line_path + str(level - 1) + '/' + str(image_with_lines),cv2.IMREAD_GRAYSCALE)
                        # print 'path', best_line_path+str(level-1)+'/'+str(image_with_lines)
                        orginal_image_name = image_with_lines[0:-4]
                        new_image_output_path = new_output_path + str(c) + '/'
                        os.mkdir(new_image_output_path)
                        recompute_obj = recompute(special_cases_list)
                        # number_of_open_plans,max_lines,iter_open_plan_text_cordinate,iter_text_words,iter_text_bounding_boxes = recompute_obj.find_open_plan_area(orginal_image_name,avg_door_width,weight_list,input_image,final_output_path,new_image_output_path,iter_open_plan_text_cordinate,iter_text_words,level,iter_text_bounding_boxes,debug_mode)
                        number_of_open_plans, max_lines = recompute_obj.find_open_plan_area(self.current_directory,orginal_image_name,avg_door_width, weight_list,input_image,final_output_path,new_image_output_path,iter_open_plan_text_cordinate, iter_text_words, level,iter_text_bounding_boxes,debug_mode)
                        if number_of_open_plans > 0:
                            num_overall_plans = num_overall_plans + 1
                        c = c + 1
                        # print ('max_lines', max_lines)
                        if max_lines != 0:
                            for m_row in max_lines:
                                max_line_list.append(m_row)

                level = level + 1
                # print 'iteration time : ',time.time()-iteration_time_start

            # end_max_line_iteration = time.time()
            # print 'Iterate until complete Open plan is partitioned Time : ', end_max_line_iteration - end_max_line

            # max_line_list  = [
            #     [[[787, 1683], [787, 1365]], [[787, 1365], [245, 1048]]],
            #     [[[512, 1203], [763, 481]]],
            #     [[[588, 993], [841, 1035]]]
            # ]

            final_image_path = self.path_final_lines + str(only_image_name) + '-' + str(room_num) + '.png'
            bogus_best_line_path = 'bogus_path/'
            line_opt = line_optimization_class(final_image_path, bogus_best_line_path, only_image_name)
            new_max_lines = line_opt.find_closest_contour_point(max_line_list, open_plan_contour_lines)


            # --append to all open plan lines
            all_open_plan_lines.append(new_max_lines)
            #--write open path lne data for evaluation
            floor_plan = cv2.imread(open_plan_path + room, cv2.IMREAD_COLOR)

            if debug_mode:
                # ----get image to draw new max lines
                floor_plan_copy = floor_plan.copy()
                # room_extract_copy = floor_plan.copy()
                # ---take each line from max_line_list
                for r, row in enumerate(new_max_lines):
                    floor_plan_iteration_copy = floor_plan.copy()
                    center_x, center_y = 0,0
                    for l, line in enumerate(row):
                        # ---draw text label
                        for t_row in open_plan_text_cordinate:
                            cv2.circle(floor_plan_iteration_copy, (tuple(t_row)), 5, (255, 0, 0), -1)
                        # ---draw each line
                        cv2.line(floor_plan_iteration_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 255), 2,cv2.cv.CV_AA)
                        cv2.line(floor_plan_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 255), 2, cv2.cv.CV_AA)
                        # cv2.line(room_extract_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
                        x1, y1 = line[0]
                        x2, y2 = line[1]
                        center_x, center_y = (x2 + x1) / 2, (y1 + y2) / 2
                    cv2.putText(floor_plan_copy, str(r).upper(), (center_x - 10, center_y+10),cv2.FONT_HERSHEY_COMPLEX, 1,(255, 0, 0), 2, cv2.cv.CV_AA)
                    cv2.imwrite(inner_directory + str(room_num) + 'Line' + str(r) + '.png', floor_plan_iteration_copy)
                for r, row in enumerate(text_words):
                    cord1 = row[1][0]
                    cord2 = row[1][1]
                    x_position = (len(row[0]) / 2) * 25
                    cv2.putText(floor_plan_copy, str(row[0]).upper(), (cord1 - x_position, cord2 + 12), cv2.FONT_HERSHEY_COMPLEX, 1,
                                (0, 0, 0), 2, cv2.cv.CV_AA)
                    # cv2.circle(floor_plan_copy, (cord1-x_position,cord2+12), 5, (255, 0, 0), -1)
                # --write to inside folder
                cv2.imwrite(inner_directory + str(room_num) + '.png', floor_plan_copy)
                # --write to final_lines folder
                cv2.imwrite(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + '.png', floor_plan_copy)
                # --write to extract room contours
                # cv2.imwrite(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png',room_extract_copy)

            # --extract room conoturs
            room_extract_copy = floor_plan.copy()
            for r, row in enumerate(new_max_lines):
                for l, line in enumerate(row):
                    cv2.line(room_extract_copy, (tuple(line[0])), (tuple(line[1])), (0, 0, 0), 2, cv2.cv.CV_AA)
            # # --write to extract room contours
            # cv2.imwrite(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png',room_extract_copy)
            #
            # gray_room = cv2.imread(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png',cv2.IMREAD_GRAYSCALE)
            gray_room = cv2.cvtColor(room_extract_copy,cv2.COLOR_RGB2GRAY)
            ret, thresh = cv2.threshold(gray_room, 0, 255, 1)
            contours, hierachy = cv2.findContours(thresh, 1, 2)
            before_room_lines = []
            for c, cnt in enumerate(contours):
                if c != len(contours) - 1:
                    # print 'after c', c
                    before_room_lines.append([])
                    text, text_cordinate = 0, 0
                    for each_text in text_words:
                        if cv2.pointPolygonTest(cnt, tuple(each_text[1]), False) == 1:
                            text = each_text[0]
                            text_cordinate = each_text[1]
                    if text != 0 and text_cordinate != 0:
                        for row in cnt:
                            before_room_lines[c].append(row[0])
                        before_room_lines[c].append([text, text_cordinate])

            room_lines = []
            #--clean room_lines list
            for r, room_row in enumerate(before_room_lines):
                if len(room_row)>1:
                    room_lines.append(room_row)

            # --write open path line data for evaluation
            op_outer_contour = ''
            for r_count, row in enumerate(open_plan_contour_lines):
                if r_count == 0:
                    op_outer_contour = op_outer_contour + '[' + str(row[0][0]) + ' ' + str(row[0][1]) + ']'
                else:
                    op_outer_contour = op_outer_contour + ',[' + str(row[0][0]) + ' ' + str(row[0][1]) + ']'

            op_details_file.write('Plan ' + str(room_num + 1) + ' : Contour : ' + op_outer_contour + ' : Lines : ')
            for boundary_num, boundary in enumerate(new_max_lines):
                for b_line in boundary:
                    if boundary_num == len(new_max_lines) - 1:
                        op_details_file.write('[' + str(b_line[0]) + ' ' + str(b_line[1]) + ']')
                    else:
                        op_details_file.write('[' + str(b_line[0]) + ' ' + str(b_line[1]) + '], ')

            # --write data to gravvitas
            room_details_file = open(self.path_gravvitas + only_image_name + '_1_room_details.txt', 'a')
            for row in room_lines:
                no_of_rooms += 1
                room_details_file.write('open : plan ' + str(room_num + 1) + '-room ' + str(no_of_rooms) + ' : ' + str(
                    row[-1][0]) + ' : ' + str(row[-1][1]) + ' : ')
                room_details_file.write('contour_details : ')
                # --open plan file write
                op_details_file.write(' : ' + str(row[-1][0]) + ' : ' + str(row[-1][1]) + ' : room_contour_details : ')

                for cord_num, cordinate in enumerate(row[:-1]):
                    if cord_num == len(row[:-1]) - 1:
                        room_details_file.write(str(cordinate))
                        op_details_file.write(str(cordinate))
                    else:
                        room_details_file.write(str(cordinate) + ',')
                        op_details_file.write(str(cordinate) + ',')
                room_details_file.write('\n')
                room_details_file.write('\n')
            room_details_file.close()
            # os.remove(self.path_final_lines + str(only_image_name) + '-' + str(room_num) + 'room.png')
            op_details_file.write('\n')

            # print 'writing to files time : ',time.time()-end_max_line_iteration

        op_details_file.close()



    def find_open_plan_doors(self,current_directory,path_gravvitas_text,only_image_name,img_height,img_width,no_text_image_path,avg_wall_width, avg_door_width, debug_mode):

        #--1. get single use room names
        non_open_plan_rooms_lst = self.iterative_obj.get_list_from_text_files(current_directory + '/Model/floor_plan_text/op_door_find_rooms.txt')

        #--3. read open plan file
        all_op_data = self.read_open_plan_file(path_gravvitas_text,only_image_name)

        #---4. get open plan contours+ SGL rooms+their contours
        OP_data = []
        for op_row in all_op_data:
            open_plan_contour = op_row[0]
            non_op_room_data = []
            for room_data in op_row[1:]:
                room_name = room_data[0]
                if room_name in non_open_plan_rooms_lst:
                    non_op_room_data.append(room_data)
            if len(non_op_room_data) >0:
                OP_data.append([open_plan_contour,non_op_room_data])

        #---5. find op doors
        checked_partition_lines = []
        if len(OP_data)>0:
            for op_row in OP_data:
                self.identify_partition_elements(op_row,path_gravvitas_text,img_height,img_width,avg_wall_width,avg_door_width,no_text_image_path,checked_partition_lines,only_image_name)




    def read_open_plan_file(self,path_gravvitas,only_image_name):
        all_op_data = []
        room_details_file = open(path_gravvitas + only_image_name + '_8_openPlan_details.txt', 'r')
        for op_number, text_line in enumerate(room_details_file):
            contour_seperate = text_line.split(':')
            if len(contour_seperate)>3:
                # ---get op contour points
                contour_details = re.findall(r"[\w']+", contour_seperate[2])
                int_contour_details = [int(x) for x in contour_details]
                open_plan_contour_points = [int_contour_details[x:x + 2] for x in xrange(0, len(int_contour_details), 2)]
                open_plan_cont = self.iterative_obj.convert_points_to_contour(open_plan_contour_points)
                all_op_data.append([open_plan_cont])

                for n in np.arange(8,len(contour_seperate),4):
                    room_name = contour_seperate[n-3].strip()
                    room_cont_str = re.findall(r"[\w']+", contour_seperate[n])
                    room_cont_int = [int(x) for x in room_cont_str]
                    room_cont_cords = [room_cont_int[x:x + 2] for x in xrange(0, len(room_cont_int), 2)]
                    room_cont = self.iterative_obj.convert_points_to_contour(room_cont_cords)
                    all_op_data[-1].append([room_name,room_cont])

        return all_op_data


    def identify_partition_elements(self,op_row,path_gravvitas_text,height,width,avg_wall_width,avg_door_width,no_text_image_path,checked_partition_lines,only_image_name):
        #---1. define thresholds
        door_width = avg_wall_width/2
        avg_internal_door_area = (avg_door_width*avg_wall_width)/2
        op_contour,room_list = op_row

        connection_list = []
        #-- each Op_row can have >1 SGL room
        for room in room_list:
            print room[0]
            #---2. erase room walls leaving only partition elements
            room_image = ~(np.zeros((height, width, 3), np.uint8))
            cv2.drawContours(room_image, [room[1]], -1, (0, 0, 0), door_width)
            cv2.drawContours(room_image, [op_contour], -1, (255,255,255), door_width+3)
            # cv2.imwrite(path_gravvitas_text + 'op.png', room_image)

            #--3. get all elements point list from erased image
            elements_point_list = self.find_element_contour(room_image,path_gravvitas_text,height, width,avg_door_width,avg_internal_door_area)

            #--4. calculate element confidence
            for element_row in elements_point_list:
                selected_line = self.find_longest_leg(element_row,avg_door_width)
                checked_partition_lines, already_identified = self.check_if_already_identified(selected_line,checked_partition_lines,avg_door_width)
                if already_identified == False:
                    identified_element_name, rect_points,confidence = self.identify_current_element(avg_door_width,selected_line,height,width,element_row,no_text_image_path,path_gravvitas_text)

                    # --5. add detection to relevant text files
                    self.add_to_text_files(path_gravvitas_text,only_image_name,identified_element_name,rect_points,confidence)



    def find_element_contour(self,room_image,path_gravvitas_text,height, width,avg_door_width,avg_internal_door_area):
        gray_1 = cv2.cvtColor(room_image, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_1, 0, 255, 1)
        contours, hierachy = cv2.findContours(thresh, 1, 2)

        #--find corner points of door contour
        elements_point_list = []
        for c_num, cnt in enumerate(contours):
            if abs(cv2.contourArea(cnt)-avg_internal_door_area) < cv2.contourArea(cnt):
                door_image = ~(np.zeros((height, width, 3), np.uint8))
                cv2.drawContours(door_image, [cnt], -1, (0, 0, 0), -1)
                gray = cv2.cvtColor(door_image, cv2.COLOR_RGB2GRAY)
                # cv2.imwrite(path_gravvitas_text +'Corner Points-1.png', gray)


                # --cv2.goodFeaturesToTrack(input gray image, max no of corners we need,
                # -- accuracy of corners we need, min_distance between corners)
                # --- 1. 'max no of corners we need' - can be increased to any number
                # --- 2. 'min_distance between corners' is set to 0, coz
                # --- since 'accuracy of corners we need' controls no of points
                # --- 'accuracy of corners we need' is 0.27 coz we need slanted corners too
                corners = cv2.goodFeaturesToTrack(gray, 200, 0.27, avg_door_width/10)
                corners = np.int0(corners)
                new_corners = []
                simple_cont2 = ~(np.zeros((height, width, 3), np.uint8))
                cv2.drawContours(simple_cont2, [cnt], -1, (0, 0, 0), -1)
                for i in corners:
                    x, y = i.ravel()
                    new_corners.append([x, y])
                    cv2.circle(simple_cont2, (x, y), 10, (0, 0, 255), -1)
                # cv2.imwrite(path_gravvitas_text + 'Corner Points-2.png', simple_cont2)


                # cont_point_list = self.iterative_obj.get_points_from_contour(cnt)
                # for num, r in enumerate(cont_point_list):
                #     cont_point_list[num].append([])

                # # ----order corner points based on contour
                # for p_index, point in enumerate(new_corners):
                #     x1, y1 = point
                #     closest_index, min_distance = -1, 1000
                #     for index, cont_point in enumerate(cont_point_list):
                #         x2, y2 = cont_point[0], cont_point[1]
                #         distance = math.hypot(x2 - x1, y2 - y1)
                #         if distance < min_distance:
                #             closest_index = index
                #             min_distance = distance
                #     new_corners[p_index].append(closest_index)
                # new_corners.sort(key=itemgetter(2))
                #
                # for row in new_corners:
                #     contable_corner_points.append([row[0], row[1]])
                cx,cy = self.iterative_obj.get_centre_of_contour(cnt)
                sorted_points = self.iterative_obj.order_polygon_points(new_corners,[cx,cy])
                elements_point_list.append(sorted_points)


        # ordered_contable_corner_points = self.iterative_obj.order_polygon_points(contable_corner_points)

        return elements_point_list


    def find_longest_leg(self,contable_corner_points,avg_door_width):
        lines_list = self.iterative_obj.convert_points_to_lines(contable_corner_points)
        new_lines_list = []
        for row in lines_list:
            new_lines_list.append([row])

        for l_row,line in enumerate(new_lines_list):
            x1,y1 = line[0][0]
            x2,y2 = line[0][1]
            length = math.hypot(x2-x1,y2-y1)
            m,c = self.iterative_obj.find_mc_of_line(x1,y1,x2,y2)
            new_lines_list[l_row].append(round(length,2))
            new_lines_list[l_row].append([m,c])

        new_lines_list.sort(key=itemgetter(1), reverse=True)

        longest_two_lines = new_lines_list[:2]

        m1 = longest_two_lines[0][-1][0]
        length1 = longest_two_lines[0][1]
        m2 = longest_two_lines[1][-1][1]
        length2 = longest_two_lines[1][1]

        if m1== 'a' and m2 != 'a' and abs(length2-length1) > avg_door_width/5:
            print 'OP ELEMENT IDENTIFY, TWO LINES NOT SIMILAR'
        elif m1 != m2 and abs(length2-length1) > avg_door_width/5:
            print 'OP ELEMENT IDENTIFY, TWO LINES NOT SIMILAR'


        return longest_two_lines[0][0]

    def check_if_already_identified(self,selected_line,checked_lines,avg_door_width):
        already_identified = False
        if len(checked_lines)==0:
            checked_lines.append(selected_line)
        else:
            cx,cy = self.iterative_obj.find_centre_of_line(selected_line)
            for row in checked_lines:
                cx1, cy1 = self.iterative_obj.find_centre_of_line(row)
                distance = math.hypot(cx-cx1,cy-cy1)
                if distance < (avg_door_width/10):
                    already_identified = True
                    break

        if already_identified == False:
            checked_lines.append(selected_line)


        return checked_lines,already_identified

    def identify_current_element(self,avg_door_width,connection_line,height,width,element_row,no_text_image_path,path_gravvitas_text):
        self.close_cont_detect_elements_obj = close_cont_detect_elements_class(avg_door_width, '', width, height)

        no_text_image = cv2.imread(no_text_image_path,cv2.IMREAD_COLOR)
        wall_confidence = self.close_cont_detect_elements_obj.check_rect_is_wall(element_row,no_text_image,path_gravvitas_text)

        if wall_confidence == 1:
            identified_element_name = 'wl'
            confidence = wall_confidence
        else:
            x1, y1 = connection_line[0]
            x2, y2 = connection_line[1]
            # ---get length of connection
            connection_length = math.hypot(x1 - x2, y1 - y2)

            # ---find rectangle for element by drawing paralell lines
            rect_points = self.iterative_obj.get_rectangle_from_line(connection_line, avg_door_width)

            total_door_confidence = self.check_rect_is_door(rect_points, no_text_image,path_gravvitas_text,connection_length,avg_door_width)

            # print 'total_door_confidence',total_door_confidence
            if total_door_confidence > 0.8:
                identified_element_name = 'd'
                confidence = total_door_confidence
            else:
                identified_element_name = 'un'
                confidence = 0.3

        rect_points = self.iterative_obj.get_rectangle_from_line(connection_line,avg_door_width/10)

        return identified_element_name,rect_points,confidence



    def check_rect_is_door(self,sorted_points,org_image,path_output,connection_length, avg_door_width):
        door_confidence_1 = self.check_is_door_by_dw(connection_length, avg_door_width)
        #---check door by dimensions == square -> is_door1
        door_confidence_2, imCrop = self.check_rect_is_door_by_dimension(sorted_points, org_image, path_output)
        # -- is_door_1 ==-1 means a invalid connection -that was too small to get
        #--check if black_pixel_count/bb_area < thresh -> is_door2
        door_confidence_3 = self.check_rect_is_door_by_blck_pixels(imCrop, path_output)
        total_door_confidence = (door_confidence_1+door_confidence_2+door_confidence_3)/3

        return total_door_confidence

    def check_is_door_by_dw(self,connection_length,avg_door_width):
        difference = abs(avg_door_width-connection_length)

        if difference < (avg_door_width/3.5):
            door_confidence = 1
        elif difference > (avg_door_width/3.5) and difference <  (avg_door_width/2):
            door_confidence = 0.6
        else:
            door_confidence = 0.3

        return door_confidence

    def check_rect_is_door_by_dimension(self,rectangle,org_image,path_output):
        door_confidence = 0
        imCrop = self.close_cont_detect_elements_obj.crop_image_by_rectangle(rectangle,org_image,path_output,wall=False)
        # cv2.imwrite(path_output +'-'+str(rectangle[0][0])+ '-door-temp.png', imCrop)
        # crop_height, crop_width, d = imCrop.shape

        gray_image = cv2.cvtColor(imCrop, cv2.COLOR_RGB2GRAY)
        ret, thresh = cv2.threshold(gray_image, 127, 255, 0)
        contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

        max_contour,max_area,width,height = 0,0,0,0
        for c,cnt in enumerate(contours):
            rect = cv2.minAreaRect(cnt)
            # box = cv2.cv.BoxPoints(rect)
            # box = np.int0(box)
            # print cv2.contourArea(cnt)
            if cv2.contourArea(cnt)> max_area:
                max_area = cv2.contourArea(cnt)
                width,height = rect[1]
                # max_contour = cnt

            # current_cont = ~(np.zeros((crop_height, crop_width, 3), np.uint8))
            # cv2.drawContours(current_cont, [cnt], -1, (0, 0, 0), 2)
            # print str(rectangle[0][0])+'-'+str(c)+'-'+str(cv2.contourArea(cnt))
            # cv2.imwrite(path_output+'-'+str(rectangle[0][0])+'-'+str(c)+'.png',current_cont)

        if len(contours)>0 and max_area>0:
            # current_cont = ~(np.zeros((crop_height, crop_width, 3), np.uint8))
            # cv2.drawContours(current_cont, [max_contour], -1, (0, 0, 0), 2)
            # cv2.imwrite(path_output + 'temp2.png', current_cont)



            # print str(rectangle[0][0]),'----',len(contours)
            # cv2.imwrite(path_output+'-'+str(rectangle[0][0])+'temp.png', imCrop)



            min_dimension = min(height,width)
            max_dimension = max(width,height)

            ratio = min_dimension/float(max_dimension)
            # print str(rectangle[0][0]),'---',ratio

            if ratio > 0.4:
                door_confidence = 1
            elif ratio < 0.4 and ratio > 0.2:
                door_confidence = 0.6
            else:
                door_confidence = 0.3


        return door_confidence,imCrop

    def check_rect_is_door_by_blck_pixels(self,imCrop,path_output):
        door_confidence = 0
        imcrop_height, imcrop_width, imcrop_d = imCrop.shape
        # imSize = imcrop_height * imcrop_width

        if imcrop_height != 0 and imcrop_width != 0:
            cv2.imwrite(path_output + '/temp.png', imCrop)

            # --get colors in rectangle
            newimg = Image.open(path_output + '/temp.png')
            os.remove(path_output + '/temp.png')
            color_list = newimg.getcolors()

            total_pixels = sum([row[0] for row in color_list])
            # --if has high black pixels amount : then is wall
            for col_row in color_list:
                if (0, 0, 0) in col_row:
                    black_pixel_ratio = col_row[0]/float(total_pixels)
                    if black_pixel_ratio > 0.010:
                        door_confidence = 1
                    elif black_pixel_ratio > 0.005 and black_pixel_ratio <0.010:
                        door_confidence = 0.6
                    #--if black_pixel_ratio >0.040 and <0.010
                    else:
                        door_confidence = 0.3
                    break

        return door_confidence

    def add_to_text_files(self,path_gravvitas_text,only_image_name,identified_element_name,rect_points,confidence):
        door_details_file = open(path_gravvitas_text + only_image_name + '_5_door_details.txt', 'a')
        wall_details_file = open(path_gravvitas_text + only_image_name + '_7_wall_details.txt', 'a')
        unknown_details_file = open(path_gravvitas_text + only_image_name + '_9_unknown_details.txt', 'a')

        contour = ''
        for c, cord in enumerate(rect_points):
            if c == 0:
                contour = contour + '[' + str(cord[0]) + ',' + str(cord[1]) + ']'
            else:
                contour = contour + ',[' + str(cord[0]) + ',' + str(cord[1]) + ']'


        if identified_element_name == 'wl':
            selected_file = wall_details_file
            element_name = 'Wall'
        elif identified_element_name == 'd':
            selected_file = door_details_file
            element_name = 'Door'
        else:
            selected_file = unknown_details_file
            element_name = 'Unknown'

        selected_file.write(element_name+' : ' + contour + ' : [' + str(confidence) + '] \n')
        selected_file.close()