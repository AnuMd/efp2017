__author__ = 'anu'

import os, cv2
from scipy import ndimage
import numpy as np

from room import Room
from graphCC import GraphCC
import imageOperations as imop
from connectedcomponent import CC
from objectMatching import ObjectMatching


class object_recog_functions:
    def find_objects(self, current_directory, path_output, object_path, path_gravvitas,
                     only_image_name, thresholds, avg_door_width,debug_mode):
        object_details_file = open(path_gravvitas + only_image_name + '_2_object_details.txt', 'w')

        output_path = object_path + only_image_name + '/'
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        #--testing
        # input_path = current_directory + '/input_fps/test_svg/object/'
        # #--non testng
        input_path = path_output+only_image_name+'/'

        objects_graph,groups = self.load_graphs(current_directory,'dictionary')
        noise,empty_list = self.load_graphs(current_directory,'secondary dictionary')
        secondary_graph,empty_list = self.load_graphs(current_directory,'noise')
        obj_text, group_text = self.load_graphs(current_directory,'vocabulary')




        text_coordinates = self.load_text_coordinates(input_path)
        img_red_wall = cv2.imread(input_path + 'new_red_wall.png', cv2.IMREAD_COLOR)
        img_walls_doors = cv2.imread(input_path + 'Stefano_output.png', cv2.IMREAD_COLOR)
        img_no_wall = cv2.imread(input_path + 'no_wall.png', cv2.IMREAD_GRAYSCALE)
        img_no_text = cv2.imread(input_path + 'no_text.png', cv2.IMREAD_GRAYSCALE)
        img_tmp = cv2.imread(input_path + 'Stefano_output.png', cv2.IMREAD_GRAYSCALE)
        # --get image without black colored(0) pixels
        img_tmp = cv2.inRange(img_tmp, 1, 255)
        img_no_text2 = img_no_text - img_tmp
        # --- img_no_text: open walls + objects
        # --- img_tmp : closed walls
        # --- img_no_text2: closed walls + objects (1-0 for each pixel in 2 images-> result has 1's from both images


        graph = GraphCC()
        #--add windows to graph
        graph = self.create_graph(graph, img_walls_doors, color=(0, 255, 255))
        #--add doors to graph
        graph = self.create_graph(graph, img_walls_doors, color=(0, 255, 0))
        # avg_door_width = imop.avg_doors_width(img_walls_doors)

        list_rooms = os.listdir(input_path)
        list_rooms.sort()
        rooms = []
        i = 1
        # loops over the rooms
        for ro in list_rooms:
            # ----go in to each folder '/OUTPUT/iteration_number/image_name/room_name'
            if len(ro) < 4 or ro[-4] != '.' and ro != 'external' and ro != 'discarded_cc' and ro != 'windows':
                # --takes only folders
                pa = input_path + ro + '/'
                # --takes only folders that has a graph.txt
                # --each room will have a graph.txt with sub-graphs for objects
                if os.path.exists(pa + 'graph.txt'):
                    cc = CC(-1)
                    cc.image = cv2.imread(pa + 'contour.png', cv2.IMREAD_GRAYSCALE)
                    graph = GraphCC()
                    # print 'load room '+ro
                    graph.load_graph(pa, 'graph', dictionary=False)
                    graph.label = ro
                    room = Room(i, cc, pa)
                    room.name = ro
                    room.graph = graph
                    room.set_images(img_red_wall, img_no_wall, img_no_text)
                    room.width = graph.width
                    room.set_contour()
                    rooms.append(room)
                    i += 1
        if debug_mode:
            blw = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
            blw3 = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
            blw5 = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
            blw6 = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
            blw7 = cv2.cvtColor(img_no_text2, cv2.COLOR_GRAY2RGB)
            cv2.imwrite(output_path + only_image_name + 'before_objects.png', blw7)
            font = cv2.FONT_HERSHEY_COMPLEX
            for text in text_coordinates:
                x1, y1, x2, y2 = text[2]
                cv2.putText(blw5, text[0], (x1, y1), font, 1, (0, 0, 0), 1)
        for room in rooms:
            objMatch = ObjectMatching(room, avg_door_width, text_coordinates, obj_text, group_text, thresholds)
            objMatch.secondary = secondary_graph
            objMatch.groups = groups
            objMatch.objects_graph = objects_graph
            objMatch.noise = noise
            # not very clear what the following does.
            # Probably it removes the cc corrsponding to a
            # recognized door from the list of objects in the graph?
            objMatch.remove_door_window_opening(graph)
            objMatch.search_objects()
            for cc in graph.get_indices():
                xmin, ymin, xmax, ymax = cc.get_extremes()
                if debug_mode:
                    cv2.rectangle(blw6, (xmin, ymin), (xmax, ymax), (0, 255, 0), 3)
                    cv2.putText(blw6, str(cc.id), (xmin, ymin), font, 1, (0, 0, 255), 1)
            for door in objMatch.doors:
                xmin, xmax, ymin, ymax = door.get_extremes()
                if debug_mode:
                    cv2.rectangle(blw3, (xmin, ymin), (xmax, ymax), (0, 255, 0), 3)
                    # --if you want new doors use this
                    # door_details_file.write('Door : '+str(xmin)+','+ str(ymin)+','+ str(xmax)+','+str(ymax)+'\n')
            for text_cc in objMatch.text_graph.get_indices():
                cnt = text_cc.contourSimple[0]
                for xA in cnt:
                    xA[0][0] += text_cc.x_min
                    xA[0][1] += text_cc.y_min
                if debug_mode:
                    cv2.drawContours(blw5, [cnt], 0, (0, 255, 255), 2)
            objMatch.find_table()
            # for table_found in objMatch.table:
            #     xmin, xmax, ymin, ymax = table_found.get_extremes()
            #     cv2.rectangle(blw6,(xmin, ymin), (xmax, ymax), (0,255,0), 3)
            all_obj, noises = objMatch.get_all_objects()
            for obj in all_obj:
                xmin, xmax, ymin, ymax = obj.get_extremes()
                if debug_mode:
                    cv2.rectangle(blw7, (xmin, ymin), (xmax, ymax), (255, 0, 255), 3)
                    cv2.putText(blw7, obj.label, (xmin, ymin), font, 1, (255, 0, 255), 1)

                object_details_file.write(
                    str(obj.label) + ' : ' + str(xmin) + ',' + str(ymin) + ',' + str(xmax) + ',' + str(
                        ymax) + '\n')
            for noi in noises:
                xmin, xmax, ymin, ymax = noi.get_extremes()
                if debug_mode:
                    cv2.rectangle(blw, (xmin, ymin), (xmax, ymax), (0, 255, 0), 3)
                    cv2.putText(blw, noi.label, (xmin, ymin), font, 1, (0, 0, 255), 1)

        if debug_mode:
            cv2.imwrite(output_path + only_image_name + '_text_cc.png', blw5)
            cv2.imwrite(output_path + only_image_name + '_noises.png', blw)
            cv2.imwrite(output_path + only_image_name + '_objects.png', blw7)
            cv2.imwrite(output_path + only_image_name + '_doors.png', blw3)
            cv2.imwrite(output_path + only_image_name + '_original_doors.png', blw6)

        if os.stat(path_gravvitas + only_image_name + '_2_object_details.txt').st_size == 0:
            object_details_file.write('No Objects Detected' + '\n')
        object_details_file.close()


    def load_graphs(self,current_directory,folder_name):
        path = current_directory+'/'+folder_name+'/'
        if folder_name=='dictionary':
            # --dictionary->bath001->output->1->.txt,.png
            # --A->B->C->D->E_content
            # --go in to A(dictionary)
            list_folder = os.listdir(path)
            list_folder.sort()
            groups = []
            objects_graph = []
            for folder in list_folder:
                if '.' not in folder:
                    # --folder ==B(bath001)
                    # print '--- loading '+str(folder)+' graphs ---'
                    class_path = path + folder + '/output/'
                    prop = open(path + folder + '/proportions.txt', 'r')
                    # ---in proportions file: there are N1..N6 (6 values)
                    # --N1, N2, N3, N4 are used to determine if obj has a particular width and height
                    # N1*door_width<min(obj_height, obj_width)<N2*door_width
                    # N3*door_width<max(obj_height, obj_width)<N4*door_width
                    # N5 0:if has connected walls, 1: if not
                    # N6 how many CCs are in object
                    threshes = []
                    for i in range(0, 4):
                        threshes.append(float(prop.readline()))
                    # -- N5: wall_cond is if object is connected to walls or not (1/0)
                    wall_cond = int(prop.readline())
                    # --N6: num_cc is number of connected components in object
                    num_cc = int(prop.readline())
                    line = prop.readline()
                    prop.close()
                    list_class = os.listdir(class_path)
                    list_class.sort()
                    # -- go in to C(output)
                    # --for each object inside object_folder there will be a folder in 'output' folder: D(1_folder)
                    for object in list_class:
                        # --get each objects' details from D(1_folder,etc)
                        # --'object'==1
                        object_path = class_path + object + '/'
                        list_objects = os.listdir(object_path)
                        list_objects.sort()
                        # --for each file inside D(1_folder,etc)
                        # --add all details about each object to 'objects_graph' list
                        for obj in list_objects:
                            if obj[-4:] == '.txt':
                                graph = GraphCC()
                                # --'threshes' will have min,max W and H requirements for all objects in that model
                                graph.add_threshes(threshes)
                                graph.wall_cond = wall_cond
                                # --file name will be like bath_001,bath_002,bath_003,etc inside the main folder of dictionary->bath001 (not to confuse outer bath001 and inside bath001.txt)
                                file_name = obj[0:-4]
                                # --'object_path'==dictionary_path+bath001+'/output/'+1 - this will have bath_001.txt
                                # --'object_path'==dictionary_path+bath001+'/output/'+2 - this will have bath_002.txt
                                graph.load_graph(object_path, file_name, dictionary=True)
                                graph.set_cc_to_find(num_cc)
                                graph.label = file_name
                                graph.img_no_text = cv2.imread(path + folder + '/' + file_name + '.png')
                                objects_graph.append(graph)
                # --if file is 'bath_group.txt'
                elif folder[-9:] == 'group.txt':
                    group = open(path + folder, 'r')
                    # --group=='bath_group.txt'
                    item = [folder[0:-4]]
                    # --item=="item ['bath_group']"
                    line = group.readline()
                    # --continue until line=='secondary'
                    while line[0:3] != 'sec':
                        line = line[0:-1]
                        # ==line =='shower'
                        item.append(line)
                        # --continue to read each line in file
                        line = group.readline()
                    # --by here it item will be "item ['bath_group', 'bath', 'shower', 'toilet', 'sink']"
                    # --get last read line in file =='sink'
                    line = group.readline()
                    while len(line) > 2:
                        line = 'sec' + line[0:-1]
                        # --line ='secsink'
                        item.append(line)
                        line = group.readline()
                    # --by here item will have these values:"item ['bath_group', 'bath', 'shower', 'toilet', 'sink', 'secsink', 'secbath', 'sectable']"
                    groups.append(item)
            return objects_graph, groups

        elif folder_name=='secondary dictionary':
            list_folder = os.listdir(path)
            list_folder.sort()
            secondary_graph = []
            for folder in list_folder:
                # print '--- loading '+str(folder)+' graphs ---'
                class_path = path + folder + '/output/'
                prop = open(path + folder + '/proportions.txt', 'r')
                threshes = []
                for i in range(0, 4):
                    threshes.append(float(prop.readline()))
                wall_cond = int(prop.readline())
                num_cc = int(prop.readline())
                rule = str(prop.readline())
                if rule[-1] == '\n':
                    rule = rule[0:-1]
                prop.close()
                list_class = os.listdir(class_path)
                list_class.sort()
                for object in list_class:
                    object_path = class_path + object + '/'
                    # print 'object_path = '+object_path
                    list_objects = os.listdir(object_path)
                    list_objects.sort()
                    for obj in list_objects:
                        if obj[-4:] == '.txt':
                            graph = GraphCC()
                            #--'threshes' will have min,max W and H requirements for all objects in that model
                            graph.add_threshes(threshes)
                            graph.wall_cond=wall_cond
                            #--file name will be like bath_001,bath_002,bath_003,etc inside the main folder of dictionary->bath001 (not to confuse outer bath001 and inside bath001.txt)
                            file_name = obj[0:-4]
                            #--'object_path'==dictionary_path+bath001+'/output/'+1 - this will have bath_001.txt
                            #--'object_path'==dictionary_path+bath001+'/output/'+2 - this will have bath_002.txt
                            graph.load_graph(object_path, file_name, dictionary=True)
                            graph.set_cc_to_find(num_cc)
                            graph.label = file_name
                            graph.rule = rule
                            graph.img_no_text = cv2.imread(path + folder + '/' + file_name + '.png')
                            secondary_graph.append(graph)
            return secondary_graph, []

        elif folder_name=='noise':
            # --noise is about matching with 'Stairs': since noise folder has only stairs folder
            list_folder = os.listdir(path)
            list_folder.sort()
            noise = []
            for folder in list_folder:
                # print '--- loading '+str(folder)+' graphs ---'
                class_path = path + folder + '/output/'
                prop = open(path + folder + '/proportions.txt', 'r')
                threshes = []
                for i in range(0, 4):
                    threshes.append(float(prop.readline()))
                wall_cond = int(prop.readline())
                num_cc = int(prop.readline())
                line = prop.readline()
                prop.close()
                list_class = os.listdir(class_path)
                list_class.sort()
                for object in list_class:
                    object_path = class_path + object + '/'
                    # print 'object_path = '+object_path
                    list_objects = os.listdir(object_path)
                    list_objects.sort()
                    for obj in list_objects:
                        if obj[-4:] == '.txt':
                            graph = GraphCC()
                            graph.add_threshes(threshes)
                            graph.wall_cond = wall_cond
                            file_name = obj[0:-4]
                            graph.load_graph(object_path, file_name, dictionary=True)
                            graph.set_cc_to_find(num_cc)
                            graph.label = file_name
                            graph.img_no_text = cv2.imread(path + folder + '/' + file_name + '.png')
                            noise.append(graph)
            return noise,[]

        elif folder_name == 'vocabulary':
            list_txt = os.listdir(path)
            list_txt.sort()
            group_text = []
            obj_text = []
            for txt in list_txt:
                if txt[-4:] == '.txt':
                    file = open(path + txt, 'r')
                    word = file.readline()
                    word = word[0:-1]
                    text = [txt[0:-4]]
                    while word.lower() != 'end':
                        text.append(word.lower())
                        word = file.readline()
                        word = word[0:-1]
                    file.close()
                    if 'group' in text[0]:
                        group_text.append(text)
                    else:
                        obj_text.append(text)
            return obj_text, group_text


    def load_text_coordinates(self, path):
        file = open(path+'/recognized_text.txt', 'r')
        num = int(file.readline())
        text_coordinates = []
        for k in range(0, num):
            text = []
            name = str(file.readline())
            while name[-1] == '\n':
                name = name[0:-1]
            text.append(name)
            cx = int(file.readline())
            cy = int(file.readline())
            text.append([cx,cy])
            x1 = int(file.readline())
            y1 = int(file.readline())
            x2 = int(file.readline())
            y2 = int(file.readline())
            text.append([x1,y1,x2,y2])
            text_coordinates.append(text)
        file.close()
        return text_coordinates


    def create_graph(self, graph, img_walls_and_doors, color):
        img = cv2.inRange(img_walls_and_doors, color, color)
        labels, numlabels = ndimage.label(img>127)
        offset = graph.size()
        for i in range(1,numlabels+1):
            cc=CC(offset+i)
            point= np.argwhere(labels == i)
            # find x_max, x_min, y_max, y_min
            cc.y_min=point[:,0].min()
            cc.x_min=point[:,1].min()
            cc.y_max=point[:,0].max()
            cc.x_max=point[:,1].max()


            #I create image of the connected compis
            # an open image with black background and white lines,
            # i want as output an image with white background and black linesonent
            cc.image=np.where(labels[cc.y_min:cc.y_max+1,cc.x_min:cc.x_max+1] != i, 0, 255)
            cc.image = imop.add_pad_single_pixel(cc.image)
            cc.set_coordinates()
            # cc.image_dist_tran=ndimage.morphology.distance_transform_cdt(cc.image)
            cc.find_contour()
            cc.numPixel=(cc.image.sum()/255)
            if cc.area>40:
                cc.find_contour()
                # I add the connected component to the list
                graph.add_node(cc)
        return graph

    def read_vocabulary(self, vocabulary_path):
        list_txt = os.listdir(vocabulary_path)
        list_txt.sort()
        group_text = []
        obj_text = []
        for txt in list_txt:
            if txt[-4:] == '.txt':
                file = open(vocabulary_path + txt, 'r')
                word = file.readline()
                word = word[0:-1]
                text = [txt[0:-4]]
                while word.lower() != 'end':
                    text.append(word.lower())
                    word = file.readline()
                    word = word[0:-1]
                file.close()
                if 'group' in text[0]:
                    group_text.append(text)
                else:
                    obj_text.append(text)
        return obj_text, group_text
