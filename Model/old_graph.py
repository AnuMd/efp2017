__author__ = 'stefano'

import numpy as np
from node import Node

class Graph(object):
    def __init__(self):
        self._nodes = []
        self._indices = {}

    def get_node(self, u):
        return self._nodes[self._indices[u]]

    def add_node(self, u):
        if u not in self._indices:
            self._nodes.append(u)
            self._indices[u] = len(self._nodes) - 1
        return self.get_node(u)

    def add_edge(self, u, v, type):
        node_u = self.get_node(u)
        node_v = self.get_node(v)
        node_v.add_adjacent(self.get_node(u), type)
        node_u.add_adjacent(self.get_node(v), type)

    def nodes(self):
        return self._nodes[:]

    def remove_node2(self,u):
        if u in self._indices:
            self._nodes.remove(u)
            self._indices[u] = None

    def remove_node(self,u):
        if u in self._indices:
            self._nodes.remove(u)
            for n in self._nodes[self._indices[u]:len(self._nodes)]:
                n.id-=1
                self._indices[n] = n.id
            del self._indices[u]

    def merge_nodes(self,n1,n2):

        # cambio le coordinate
        x1_min,y1_min,x1_max,y1_max = n1.get_coordinate()
        x2_min,y2_min,x2_max,y2_max = n2.get_coordinate()




        if (x1_min > x2_min):
            x_min = x2_min
        else:
            x_min = x1_min

        if (x1_max < x2_max):
            x_max = x2_max
        else:
            x_max = x1_max

        if (y1_min > y2_min):
            y_min = y2_min
        else:
            y_min = y1_min

        if (y1_max < y2_max):
            y_max = y2_max
        else:
            y_max = y1_max

        n1.set_coordinate(x_min,y_min,x_max,y_max)



        #copio le liste di adiacenza e aggiorno le liste degli adiacenti
        # vado a creare un arco tra

        l1 = n2.adjacent()
        for n in l1:
            if n1.id != n.id:
                self.add_edge(n,n1,'ADIACENT')
            n.remove_adjacent(n2)

        l2 = n2.tangent()
        for n in l2:
            if n1.id != n.id:
                self.add_edge(n,n1,'TANGENT')
            n.remove_tangent(n2)

        l3 = n2.aligned()
        for n in l3:
            if n1.id != n.id:
                self.add_edge(n,n1,'ALIGNED')
            n.remove_aligned(n2)

        # estendo la lista dei punti di n1 includento quelli di n2

        n1.extend_list_point(n2.get_list_point)

        # modifico immaggine di n1 aggiungendo immagine di n2

        i1=n1.get_image()
        i2=n2.get_image()
        i = np.zeros((y_max-y_min+1,x_max-x_min+1),dtype=np.uint8)
        i[y1_min-y_min:y1_max-y1_min+1,x1_min-x_min:x1_max-x1_min+1]=i1
        i[y2_min-y_min:y2_max-y2_min+1,x2_min-x_min:x2_max-x2_min+1]=i2
        self.remove_node(n2)