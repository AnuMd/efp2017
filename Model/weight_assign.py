__author__ = 'anu'

class weight_define:

    def line_select(self, max_list_type,max_line_data,seperated_voronoi_lines,seperated_edge_lines,improved_voronoi_lines,iteration):
        voronoi_weight_test = [
            [0,[[False]]],
            [1,[[False],[False]]],
            [2,[[False]]],
            [3,[[False]]]
        ]

        edge_weight_test = [
            [0,[[[False]]]],
            [1,[[[False]],[[False]]]],
            [2,[[[True]]]],
            [3,[[[True],[True]]]]
        ]

        imporved_voronoi_weight_test =[
            [0,[[[True],[True],[True]]]],
            [1,[[[False],[False],[False],[False],[False],[False],[False],[False],[False],[False],[False],[False],[False],[False],[False]],[[True],[True],[True]]]],
            [2,[[[False],[False],[False],[False],[False],[False],[False]]]],
            [3,[[[False],[False],[False]]]],
        ]
        all_best_lines = []

        if iteration ==1:
            for row_num,row in enumerate(voronoi_weight_test):
                for boundary_num,boundary in enumerate(row[1]):
                    if boundary[0]==True:
                        voronoi_line_row=[]
                        for row in seperated_voronoi_lines:
                            if row[0]==row_num:
                                voronoi_line_row=row[1]
                                break
                        voronoi_lines = voronoi_line_row[boundary_num]
                        all_best_lines.append(voronoi_lines)
            for row_num,row in enumerate(edge_weight_test):
                for boundary_num,boundary in enumerate(row[1]):
                    for line_number, each_line in enumerate(boundary):
                        if each_line[0]==True:
                            edge_line_set=[]
                            for row in seperated_edge_lines:
                                if row[0]==row_num:
                                    edge_line_set=row[1]
                                    break
                            edge_boundary_scores = edge_line_set[boundary_num]
                            edge_lines = edge_boundary_scores[line_number]
                            all_best_lines.append([edge_lines[0]])
            for row_num,row in enumerate(imporved_voronoi_weight_test):
                for boundary_num,boundary in enumerate(row[1]):
                    for line_number, each_line in enumerate(boundary):
                        if each_line[0]==True:
                            improved_line_row=[]
                            for row in improved_voronoi_lines:
                                if row[0]==row_num:
                                    improved_line_row=row[1]
                                    break
                            improved_boundary_scores = improved_line_row[boundary_num]
                            improved_max_line_set = improved_boundary_scores[line_number]
                            all_best_lines.append(improved_max_line_set)

            print '--------all_best_lines------------'
            for row in all_best_lines:
                print row

            value = 0
            if max_list_type == 'V':
                voronoi_line_row =[]
                for row in voronoi_weight_test:
                    if row[0]==max_line_data[0]:
                        voronoi_line_row=row[1]
                        break
                value = voronoi_line_row[max_line_data[1]]
            elif max_list_type == 'E':
                edge_line_set=[]
                for row in edge_weight_test:
                    if row[0]==max_line_data[0]:
                        edge_line_set=row[1]
                        break
                edge_boundary = edge_line_set[max_line_data[1]]
                value = edge_boundary[max_line_data[2]]
            elif max_list_type == 'I':
                improved_line_row=[]
                for row in imporved_voronoi_weight_test:
                    if row[0]==max_line_data[0]:
                        improved_line_row=row[1]
                        break
                improved_boundary_scores = improved_line_row[max_line_data[1]]
                value = improved_boundary_scores[max_line_data[2]]
            if value[0]:
                return True,all_best_lines
            else:
                return False,all_best_lines


    def recompute_line_select(self,major_max_lines,all_best_lines):
        value = False
        for each_line_set in major_max_lines:
            each_line = each_line_set[0]
            x1,y1 = each_line[0]
            x2,y2 = each_line[1]
            for each_best_line_row in all_best_lines:
                for each_line in each_best_line_row:
                    x3,y3 = each_line[0]
                    x4,y4 = each_line[1]
                    if (abs(x1-x3)<2 and abs(y1-y3)<2 and abs(x2-x4)<2 and abs(y2-y4)<2) or (abs(x1-x4)<2 and abs(y1-y4)<2 and abs(x2-x3)<2 and abs(y2-y3)<2):
                        value = True
                        break

        return value