__author__ = 'stefano'


class Edge(object):
    def __init__(self, id,n,t):
        #id dell'arco avra come valore nodo.id*100+len(self._list_edge)
        self.id = id
        self._node = n
        self._type = t


    def set_type(self, t):
        self._type = t

    def get_type(self):
        return  self._type

    def set_node(self, n):
        self._node = n

    def get_node(self):
        return  self._node


 # redefines the operator ==
    def __eq__(self, x):
        return x.id == self.id

    # redefines the operator !=
    def __ne__(self, x):
        return not (x == self)

    # redefines the operator "in" for set
    def __hash__(self):
        return self.id.__hash__()