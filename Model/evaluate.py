import cv2
from evaluate_functions import evaluate_functions_class

class evaluation_class:
    def __init__(self):
        self.evaluate_functions_obj = evaluate_functions_class()

    def conduct_evaluation(self,orginal_img_height, orginal_img_width,path_gravvitas,path_gt,evaluation_output_path,file_name):
        only_image_name = file_name[0:-4]
        #--area comparison
        all_op_data = self.evaluate_functions_obj.extract_poplar_room_data(path_gravvitas,only_image_name)
        gt_room_data = self.evaluate_functions_obj.extract_gt_room_data(path_gt)
        op_area_list = self.evaluate_functions_obj.find_gt_for_rooms(all_op_data,gt_room_data)
        #--JI calculation
        average_op_JI = self.evaluate_functions_obj.compare_poplar_and_gt_JI(orginal_img_height, orginal_img_width,op_area_list,evaluation_output_path)
        #--core region index calculation
        average_op_CR = self.evaluate_functions_obj.compare_poplar_and_gt_CR(orginal_img_height, orginal_img_width,
                                                                             op_area_list, evaluation_output_path)
        # #--line comparison
        op_data = self.evaluate_functions_obj.extract_poplar_op_data(path_gravvitas, only_image_name,1500, 2000,evaluation_output_path)
        gt_line_data = self.evaluate_functions_obj.extract_gt_line_data(path_gt)
        line_list = self.evaluate_functions_obj.find_gt_for_lines(op_data, gt_line_data)
        #--average pixel ratio calculation
        average_op_pixel_ratio = self.evaluate_functions_obj.compare_poplar_and_gt_lines(orginal_img_height, orginal_img_width,
                                                                                    line_list, evaluation_output_path)

        # print 'average_op_JI',average_op_JI
        # print 'average_op_CR', average_op_CR

        #--assign weight we found
        ji_weight, cr_weight = 0.5, 0.5
        weighted_JI = average_op_JI*ji_weight
        weighted_CR = average_op_CR*cr_weight
        image_score = weighted_JI + weighted_CR
        # print 'IMAGE SCORE', image_score

        return average_op_JI,average_op_CR,average_op_pixel_ratio,weighted_JI,weighted_CR,image_score