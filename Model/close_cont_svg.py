import cv2
from close_contour import contour_closing_functions

class close_cont_svg_class():

    def __init__(self):
        pass

    def close_contour(self,path_gravvitas,only_image_name,path_output,img_height,
                      img_width,avg_wall_width,avg_door_width,debug_mode):
        contour_closing_class_obj = contour_closing_functions(avg_wall_width,avg_door_width)

        rect_list = contour_closing_class_obj.get_all_component_list(path_gravvitas, only_image_name,path_output, img_height, img_width, debug_mode)
        rect_outer_cont_list = contour_closing_class_obj.get_outer_cont_rects(rect_list,use_wall_width=False)
        outer_rect_image = contour_closing_class_obj.draw_outer_rects(img_height, img_width,rect_outer_cont_list,path_output + only_image_name, debug_mode)
        disconnected_cont_list = contour_closing_class_obj.find_opened_contours(outer_rect_image, path_output + only_image_name,img_height, img_width, debug_mode,svg=True)

        # -- Only if there are any opened contours, try to close the door
        all_doors, outer_cont, connection_rects = [], 0, []
        if len(disconnected_cont_list) > 0:
            line_lists, simplified_contours = contour_closing_class_obj.get_bbxes_all_outer_conts(outer_rect_image,path_output + only_image_name, img_height,img_width,disconnected_cont_list, debug_mode)
            border_point_data = contour_closing_class_obj.get_points_on_image_border(img_height, img_width,outer_rect_image, path_output + only_image_name,debug_mode)
            outermost_cont_lines = contour_closing_class_obj.get_outermost_cont_lines(border_point_data,line_lists, outer_rect_image, path_output + only_image_name,img_height, img_width, debug_mode)
            shortest_line_sets, shortest_conts = contour_closing_class_obj.find_short_line_segs(line_lists,img_height, img_width,path_output + only_image_name, debug_mode,svg=True)
            main_shortest_conts = shortest_conts.copy()
            # --out of all short walls, select ones in outer contour
            outer_cont_shortest_seg_list, shortest_conts = contour_closing_class_obj.find_outer_cont_shortest_segs(shortest_line_sets,outermost_cont_lines,shortest_conts,path_output + only_image_name,debug_mode)
            # --sort lines by length
            sorted_shortest_lines = contour_closing_class_obj.order_shortest_lines(outer_cont_shortest_seg_list,line_lists, shortest_conts,path_output + only_image_name, debug_mode)

            # ----1. close contour method 01--------------------------
            door_line_list_1, connections = contour_closing_class_obj.close_contour_method_01(sorted_shortest_lines, line_lists,simplified_contours,shortest_conts, path_output + only_image_name, debug_mode,svg=True)
            door_rect_as_points = contour_closing_class_obj.create_doors(door_line_list_1, [])
            all_doors = door_line_list_1
            # ---find wall ends that didnt make a connection
            closed_contour_flag = contour_closing_class_obj.check_conotour_close_bb_area_measure(door_rect_as_points,simplified_contours, img_height, img_width,path_output + only_image_name)

            # ----2. close contour method 02--------------------------
            if closed_contour_flag == False:
                #     # print 'M2'
                door_line_list_2 = contour_closing_class_obj.close_contour_method_02(connections, line_lists, sorted_shortest_lines,simplified_contours, door_rect_as_points,door_line_list_1,shortest_conts, path_output + only_image_name, debug_mode)
                door_rect_as_points = contour_closing_class_obj.create_doors(door_line_list_2, door_rect_as_points)
                all_doors = all_doors + door_line_list_2
                closed_contour_flag2 = contour_closing_class_obj.check_conotour_close_bb_area_measure(door_rect_as_points,simplified_contours, img_height,img_width,path_output + only_image_name)

                # ----3. close contour method 03--------------------------
                if closed_contour_flag2 == False:
                    # print 'M3'
                    connected_walls_2 = contour_closing_class_obj.find_connected_wall_ends(sorted_shortest_lines, all_doors, shortest_conts,path_output + only_image_name)
                    door_line_list_3 = contour_closing_class_obj.closing_method_03(connected_walls_2, sorted_shortest_lines,line_lists, simplified_contours, shortest_conts, path_output + only_image_name, debug_mode)
                    door_rect_as_points = contour_closing_class_obj.create_doors(door_line_list_3, door_rect_as_points)
                    all_doors = all_doors + door_line_list_3

                    closed_contour_flag3 = contour_closing_class_obj.check_conotour_close_bb_area_measure(door_rect_as_points,simplified_contours, img_height,img_width,path_output + only_image_name)

                    # ----4. close contour method 04--------------------------
                    if closed_contour_flag3 == False:
                        # print 'M4'
                        connected_walls_3 = contour_closing_class_obj.find_connected_wall_ends(sorted_shortest_lines,all_doors, shortest_conts,path_output + only_image_name)
                        door_line_list_4 = contour_closing_class_obj.closing_method_04(connected_walls_3, sorted_shortest_lines,line_lists, simplified_contours,shortest_conts,path_output + only_image_name, debug_mode)
                        door_rect_as_points = contour_closing_class_obj.create_doors(door_line_list_4, door_rect_as_points)
                        all_doors = all_doors + door_line_list_4

            outer_doors = contour_closing_class_obj.keep_only_outer_doors(all_doors, simplified_contours,door_rect_as_points, img_height, img_width,path_output + only_image_name)
            door_rect_as_points_final = contour_closing_class_obj.create_doors(outer_doors, [])

            rect_conts = []
            for rect in door_rect_as_points_final:
                # --to remove rects that go outside image area
                invalid = False
                for p in rect:
                    x1, y1 = p
                    if x1 < 0 or x1 > img_width or y1 < 0 or y1 > img_height:
                        invalid = True
                        break
                if invalid == False:

                    connection_rects.append(rect)
                    rect_cont = contour_closing_class_obj.iterative_obj.convert_points_to_contour(rect)

                    # --to remove too big areas and too small areas
                    rect_area = cv2.contourArea(rect_cont)
                    if rect_area > (img_height * img_width) / 20 or rect_area < (avg_door_width / 10):
                        continue
                    else:
                        rect_conts.append(rect_cont)
                        if debug_mode:
                            cv2.drawContours(main_shortest_conts, [rect_cont], -1, (0, 0, 255), -1)

            outer_cont = contour_closing_class_obj.find_outermost_contour(simplified_contours, rect_conts, img_height, img_width,path_output + only_image_name,debug_mode,svg=True)

            if debug_mode:
                cv2.imwrite(path_output + only_image_name + '-rects.png', main_shortest_conts)

        connection_data = [row[0] for row in all_doors][:len(connection_rects)]
        # old_components = rect_list

        return connection_data, outer_cont, connection_rects