__author__ = 'anu'

import cv2,time
import os
import math
import itertools
import numpy as np
import PIL.ImageOps

from PIL import Image
from operator import itemgetter

from color_check import color_check_class
from open_plan_line import line
from open_plan import planClass
from penalties_and_scores import penalties_scores_class
from iterative_functions import iterative_functions_class
from line_optimization_functions import line_optimization_function_class
from generate_other_lines import generate_other_lines_class
from recompute_functions import recompute_functions_class

class recompute:

    def __init__(self,special_cases_list):
        self.penalties_scores_obj = penalties_scores_class()
        self.iterative_obj = iterative_functions_class()
        self.special_cases_list = special_cases_list



    def find_open_plan_area(self,current_directory,orginal_image_name,avg_door_width,
                            weight_list,input_image,final_output_path,new_output_path,
                            orginal_open_plan_text_cordinate,original_text_words,
                            open_plan_level,text_bounding_boxes_original,debug_mode):
        self.line_optimization_function_obj = line_optimization_function_class()

        gray = input_image
        gray_height,gray_width = gray.shape
        open_plan_path =new_output_path+'plans/'
        os.mkdir(open_plan_path)
        region_grow_path =new_output_path+'region_grow/'
        os.mkdir(region_grow_path)
        voronoi_lines_path =new_output_path+'voronoi_lines/'
        os.mkdir(voronoi_lines_path)
        edge_extensions_path =new_output_path+'edge_extensions/'
        os.mkdir(edge_extensions_path)
        shortest_path_path =new_output_path+'shortest_path/'
        os.mkdir(shortest_path_path)
        line_optimizaiton_path =new_output_path+'line_optimizaiton/'
        os.mkdir(line_optimizaiton_path)
        line_scoring_path =new_output_path+'line_scoring/'
        os.mkdir(line_scoring_path)

        self.line_optimization_function_obj = line_optimization_function_class()

        # iteration_obj = iterative_functions_class()
        self.recompute_functions_obj = recompute_functions_class(gray_height,gray_width,open_plan_path,region_grow_path,voronoi_lines_path,edge_extensions_path,shortest_path_path,line_optimizaiton_path,line_scoring_path,avg_door_width)

        furniture_words_list, non_open_plan_rooms_words_list, wc_words_list, bath_words_list, corridor_words_list = self.special_cases_list

        all_detected_contour_lines = self.iterative_obj.find_open_plans(current_directory,input_image,new_output_path,open_plan_path,orginal_open_plan_text_cordinate,original_text_words,text_bounding_boxes_original,avg_door_width)


        if len(all_detected_contour_lines)==0:
            if debug_mode:
                print 'No Open Plan'
            # return len(all_detected_contour_lines),0,orginal_open_plan_text_cordinate,original_text_words,text_bounding_boxes_original
            return len(all_detected_contour_lines),0


        major_voronoi_data =[]
        major_all_contour_lines=[]
        major_edge_extensions=[]
        major_shortest_path_data=[]
        major_max_lines=[]
        major_edge_extensions_between_coordinates=[]
        open_area_list = os.listdir(open_plan_path)
        open_area_list.sort()

        # major_open_plan_text_cordinate,major_text_words,major_text_bounding_boxes = [],[],[]
        for common_room_number,room in enumerate(open_area_list):
            open_plan_text_cordinate,text_words,text_bounding_boxes = [],[],[]
            common_image_name = str(room)
            common_name = common_image_name[0:-4]
            room_number = common_image_name[:-4]

            current_open_plan_contour_data = []

            for contour_detail_row in all_detected_contour_lines:
                if contour_detail_row[0]==int(room_number):
                    current_open_plan_contour_data = contour_detail_row
                    break

            image_contour_lines = current_open_plan_contour_data[1]
            contour_to_check = current_open_plan_contour_data[2]
            for r,row in enumerate(current_open_plan_contour_data[3:]):
                open_plan_text_cordinate.append(row[1])
                text_words.append(row[0])
                text_bounding_boxes.append(row[2])






# -------start region grow
            outer_cont = ~(np.zeros((gray_height, gray_width, 3), np.uint8))
            cv2.drawContours(outer_cont, [contour_to_check], 0, (0, 0, 0), 1)
            only_outer_cont_image = outer_cont.copy()

            region_growed_image = self.recompute_functions_obj.region_grow_function(contour_to_check,open_plan_text_cordinate,only_outer_cont_image.copy())

            major_voronoi_data, major_all_contour_lines = self.recompute_functions_obj.find_voronoi_lines(region_growed_image,open_plan_text_cordinate,common_image_name,common_name,major_voronoi_data,major_all_contour_lines,debug_mode)

            image_edge_extensions_path = edge_extensions_path + common_name + '/'
            os.mkdir(image_edge_extensions_path)
            image_path = open_plan_path + common_image_name

            original_edges_and_extensions = self.recompute_functions_obj.extend_edges(current_open_plan_contour_data,open_plan_level,contour_to_check,image_edge_extensions_path,image_path,image_contour_lines,debug_mode)


            generate_other_lines_obj = generate_other_lines_class()
            original_edges_and_extensions, interesting_lines,contour_angle_data = generate_other_lines_obj.generate_lines_from_interesting_points(common_name, image_edge_extensions_path, original_edges_and_extensions,image_contour_lines,text_bounding_boxes, open_plan_text_cordinate,avg_door_width,gray_height,gray_width)

            major_edge_extensions.append(original_edges_and_extensions)

            major_shortest_path_data,shortest_path_data = self.recompute_functions_obj.find_shortest_path(common_name,only_outer_cont_image,major_voronoi_data,common_room_number,open_plan_text_cordinate,major_shortest_path_data,contour_to_check,debug_mode)

            major_max_lines = self.recompute_functions_obj.calculate_best_line(open_plan_path,room,common_name,major_all_contour_lines,major_voronoi_data,common_room_number,current_open_plan_contour_data,text_words,self.special_cases_list,original_edges_and_extensions,shortest_path_data,open_plan_text_cordinate,text_bounding_boxes,interesting_lines,common_name,final_output_path,orginal_image_name,room_number,major_max_lines,weight_list,only_outer_cont_image,debug_mode)

        return len(all_detected_contour_lines),major_max_lines


    def read_special_cases_files(self,current_directory):
        # --furniture list
        furniture_file = open(current_directory + '/Model/floor_plan_text/furniture_list.txt', 'r+')
        furniture_string = furniture_file.read()
        furniture_words_list = furniture_string.split()
        # --non open plan list
        non_open_plan_rooms_file = open(
            current_directory + '/Model/floor_plan_text/non_open_plan_rooms_list.txt', 'r+')
        non_open_plan_rooms_string = non_open_plan_rooms_file.read()
        non_open_plan_rooms_words_list = non_open_plan_rooms_string.split()
        # --wc_list
        wc_file = open(current_directory + '/Model/floor_plan_text/wc_list.txt', 'r+')
        wc_string = wc_file.read()
        wc_words_list = wc_string.split()
        # --bath_list
        bath_file = open(current_directory + '/Model/floor_plan_text/bath_list.txt', 'r+')
        bath_string = bath_file.read()
        bath_words_list = bath_string.split()
        # --corridor list
        corridor_file = open(current_directory + '/Model/floor_plan_text/corridor_list.txt', 'r+')
        corridor_string = corridor_file.read()
        corridor_words_list = corridor_string.split()

        return furniture_words_list, non_open_plan_rooms_words_list,wc_words_list,bath_words_list,corridor_words_list