import numpy as np
import cv2,Image,os



class tactile_page_2_class:

    def __init__(self):
        pass

    def generate_page2(self,fp_text,tactile_image_path,braille_path,current_directory,avg_door_width):
        self.current_directory = current_directory
        self.avg_door_width = avg_door_width
        # --get A4 dimesnions
        A4_image_height = 1500
        A4_image_width = int(A4_image_height / (297 / 210.0)) #-- usualy 1060
        A4_image = ~(np.zeros((A4_image_height, A4_image_width, 3), np.uint8))

        # -- height parameters setting
        extra_space = A4_image_height * 0.2
        single_ex_space_gap = extra_space / 6
        legend_start_y = single_ex_space_gap

        # --write braille word 'legend'
        new_br_image_path = self.resize_braille_label(braille_path + 'words/legend_words/','legend.png')
        legend_word = cv2.imread(new_br_image_path, cv2.IMREAD_COLOR)
        legend_w_height, legend_w_width, depth = legend_word.shape
        legend_x = (A4_image_width / 2) - (legend_w_width / 2)
        A4_image[legend_start_y:legend_start_y + legend_word.shape[0], legend_x:legend_x + legend_word.shape[1]] = legend_word
        os.remove(new_br_image_path)
        legend_word_end_y = legend_start_y + legend_w_height

        # --write legend symbol
        symbol_list_col_1 = ['wall_1','wall_2','door_1','door_2']
        symbol_list_col_2 = ['window','stairs','closet','toilet']
        symbols_extra_space = single_ex_space_gap*2
        symbols_end_y = 0
        #---write legend symbols and words in column 01
        for j in xrange(2):
            if j== 0:
                symbol_list = symbol_list_col_1
                col_start_x = 50
                symbol_word_start_x = 280
            else:
                symbol_list = symbol_list_col_2
                col_start_x = 550
                symbol_word_start_x = 780

            legend_symbols_start_y = int(legend_word_end_y + (single_ex_space_gap))

            for i in xrange(4):
                # -- draw symbol
                new_br_image_path = self.resize_braille_label(braille_path + 'symbols/legend_symbols/',symbol_list[i]+'.jpg')
                symbol_image = cv2.imread(new_br_image_path, cv2.IMREAD_COLOR)
                sym_height, sym_width, sym_depth = symbol_image.shape
                if symbol_list[i]=='closet' or symbol_list[i]=='toilet':
                    col_start_x = 650
                A4_image[legend_symbols_start_y:legend_symbols_start_y + sym_height, col_start_x:(col_start_x+sym_width)] = symbol_image
                os.remove(new_br_image_path)

                # -- draw symbol word
                new_br_image_path = self.resize_braille_label(braille_path + 'words/legend_words/',symbol_list[i] + '.jpg')
                symbol_image_word = cv2.imread(new_br_image_path, cv2.IMREAD_COLOR)
                sym_w_height, sym_w_width, sym_w_depth = symbol_image_word.shape
                # print legend_symbols_start_y,(legend_symbols_start_y + sym_w_height),sym_w_height
                # print symbol_word_start_x,(symbol_word_start_x+sym_w_width),sym_w_width
                # print A4_image_width
                A4_image[legend_symbols_start_y:(legend_symbols_start_y + sym_w_height), symbol_word_start_x:(symbol_word_start_x+sym_w_width)] = symbol_image_word

                legend_symbols_start_y = legend_symbols_start_y + sym_w_height+(symbols_extra_space/3)

                symbols_end_y = legend_symbols_start_y

        brf_start_y = symbols_end_y+single_ex_space_gap

        # ---calculate text start point
        brf_start_row_number = int(brf_start_y / (A4_image_height / 25))


        self.whole_fp_generate_brf_text(fp_text, tactile_image_path,
                                        brf_start_row_number, capital=False)


        cv2.imwrite(tactile_image_path + 'page_2.png', A4_image)


    def whole_fp_generate_brf_text(self,fp_text,tactile_image_path,brf_start_row_number,
                                   capital):
        # print fp_text
        #-- 1. if NOT room text, remove first line since it is not needed for tactile
        if capital== False:
            fp_text_list = fp_text.split('.')[1:]
            fp_text_stp_1 = '.'.join(fp_text_list)
        else:
            fp_text_stp_1 = fp_text

        #-- 2. add '#' to infront of numbers
        fp_text_stp_1_list = fp_text_stp_1.split()
        for r, word in enumerate(fp_text_stp_1_list):
            if word.strip().isdigit():
                fp_text_stp_1_list[r] = '#'+word
        fp_text_stp_2 = ' '.join(fp_text_stp_1_list)

        #-- 3. add 2 spaces to replace '..' since '..' means a para break
        fp_text_stp_2_list = fp_text_stp_2.split('..')
        fp_text_stp_3 = '.  '.join(fp_text_stp_2_list)

        #-- 4. replace numbers by ascii character
        arabic_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        ascii_numbers = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
        for n_row, num in enumerate(arabic_numbers):
            fp_text_stp_3 = fp_text_stp_3.replace(num, ascii_numbers[n_row])

        #-- 5. replace '.' and ',' with relevant ascii character
        fp_text_stp_4 = fp_text_stp_3.replace('.','4')
        fp_text_stp_5 = fp_text_stp_4.replace(',', '1')

        #-- 6. get english phrases, words and their ascii representation
        english_word_list,ascii_word_list = [],[]
        brf_file = open(self.current_directory+'/Model/floor_plan_text/braille/brf_conversion.txt')
        for text_line in brf_file:
            if len(text_line)>0:
                mapping = (text_line.strip()).split(':::')
                english_word = mapping[0].strip()
                ascii_word = mapping[1].strip()
                english_word_list.append(english_word)
                ascii_word_list.append(ascii_word)

        #-- 7. replace each phrase and variable with ascii phrases and words
        for eng_row, eng_word in enumerate(english_word_list):
            if eng_word in fp_text_stp_5:
                index_range = [fp_text_stp_5.find(eng_word),fp_text_stp_5.find(eng_word)+len(eng_word)]
                fp_text_list = list(fp_text_stp_5)
                fp_text_list[index_range[0]:index_range[1]]=list(ascii_word_list[eng_row])
                fp_text_stp_5 = ''.join(fp_text_list)

        #-- 8. for room_text instances make first letter of sentence captial(room name)
        if capital:
            fp_text_stp_8 = ',' + fp_text_stp_5
        else:
            fp_text_stp_8 =  fp_text_stp_5


        #-- 8. break to new lines by para break '  ' - 2 spaces
        fp_text_stp_8_list = fp_text_stp_8.split('   ')
        # -- 9. break each para to lines to match A4 width and write each line to file
        brf_file_to_write = open(tactile_image_path+'.brf','w')
        #--- enter new lines until we get to the point I need to have brf text
        for r in xrange(brf_start_row_number):
            brf_file_to_write.write('\n')
        for chun_no, text_chunk in enumerate(fp_text_stp_8_list):
            word_list = text_chunk.split(' ')
            letter_count,new_line_words = 0,'  '
            written_last_line = False
            for each_word in word_list:
                if len(new_line_words) + len(each_word) < 30:
                    new_line_words = new_line_words + each_word +' '
                    written_last_line = False
                else:
                    brf_file_to_write.write(new_line_words + '\n')
                    new_line_words = each_word+' '
                    written_last_line = True
            if written_last_line==False:
                brf_file_to_write.write(new_line_words + '\n')
        brf_file_to_write.close()


    def resize_braille_label(self,path,image_name):
        braile_text_image_old = cv2.imread(path+image_name, cv2.IMREAD_COLOR)
        old_height, old_width, depth = braile_text_image_old.shape
        braille_img = Image.open(path+image_name)
        new_height = int(old_height*(self.avg_door_width*0.012))
        new_width = int(old_width*(self.avg_door_width*0.012))
        new_braille_img = braille_img.resize((new_width,new_height),Image.ANTIALIAS)
        new_braille_image_name = image_name[:-4]+'_2.png'
        new_braille_img.save(path+new_braille_image_name)
        return path+new_braille_image_name
